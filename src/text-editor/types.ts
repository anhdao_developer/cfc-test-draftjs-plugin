import { isEqual, cloneDeep } from 'lodash';
import { convertFromRaw, EditorState, convertToRaw, ContentState } from './helpers';

const replaceAtRange = (s: string, start: number, end: number, substitute: string) =>
  s.substring(0, start) + substitute + s.substring(end);

const placeholderText = (characterLength: number, index: number): string => {
  let chars = '';
  for (let i = 0; i < characterLength - 1; i++) {
    chars += '$';
  }
  chars += index;
  return chars;
};

export interface RichTextEntityMap {
  [key: number]: {
    data: {
      mention: CustomField;
    };
    mutability: 'IMMUTABLE' | string;
    type: 'mention' | string;
  };
}

export interface RichTextBlock {
  data: {};
  depth: number;
  entityRanges: Array<{ offset: number; length: number; key: number; }>;
  inlineStyleRanges: Array<{}>;
  key: string;
  text: string;
  type: 'unstyled' | string;
}

export class RichTextRaw {
  blocks: RichTextBlock[];
  entityMap: RichTextEntityMap;

  static toRichText(text: string): RichTextRaw {
    const contentState = ContentState.createFromText(text);
    const editorState = EditorState.createWithContent(contentState);
    return convertToRaw(editorState.getCurrentContent());
  }

  static equals(original: RichTextRaw, compareWith: RichTextRaw): boolean {
    if (!original) {
      if (compareWith) {
        return false;
      } else {
        return true;
      }
    }
    if (!isEqual(original.entityMap, compareWith.entityMap)) {
      return false;
    }
    const plain1 = original.blocks.map(block => block.text);
    const plain2 = compareWith.blocks.map(block => block.text);
    return isEqual(plain1, plain2);
  }

  static toText(rawInput: RichTextRaw, subscriber: any): string {
    // TODO: This is dangerous
    if (!rawInput) {
      return '';
    }
    const raw = cloneDeep(rawInput);
    const unknownMentionValues: { [key: number]: { placeholder: string, fallback: string } } = {};
    const mentionValues: { [key: number]: { value: string, placeholder?: string } } = {};
    // TODO:
    // This is not very optimised because we look like 3 times through different
    // collections
    const blocks = raw.blocks.map((block, blockIndex) => {
      const updatedBlock = { ...block };
      updatedBlock.entityRanges.forEach((range, index) => {
        const placeholder = placeholderText(range.length, range.key);
        updatedBlock.text = replaceAtRange(
          updatedBlock.text,
          range.offset,
          range.offset + range.length,
          placeholderText(range.length, range.key) // Insert $$$1 placeholders
        );
        if (mentionValues[range.key]) {
          mentionValues[range.key].placeholder = placeholder;
        } else {
          unknownMentionValues[range.key] = {
            placeholder,
            fallback: '',
          };
        }
      });
      return updatedBlock;
    });
    let result = convertFromRaw({
      ...raw,
      blocks,
    }).getPlainText();
    for (const key in mentionValues) {
      if (mentionValues[key] && mentionValues[key].placeholder) {
        const mentionValue = mentionValues[key];
        const regexp = new RegExp(mentionValue.placeholder.replace(/\$/g, '\\$'), 'i');
        result = result.replace(
          regexp,
          mentionValue.value
        );
      }
    }
    for (const key in unknownMentionValues) {
      if (unknownMentionValues[key] && unknownMentionValues[key].placeholder) {
        const mentionValue = unknownMentionValues[key];
        const regexp = new RegExp(mentionValue.placeholder.replace(/\$/g, '\\$'), 'i');
        result = result.replace(
          regexp,
          mentionValue.fallback
        );
      }
    }
    return result;
  }
}

export enum ECustomFieldType {
  Number = 'num',
  Text = 'txt',
  Coords = 'coords',
}

export class CustomField {
  id: string;
  name: string;
  type: ECustomFieldType;
  capital: string;
}
export type OwnProps = {
  onChange: (raw: RichTextRaw) => void;
  defaultValue: RichTextRaw;
  value?: RichTextRaw;
  placeholder: string;
  roundTopLeft?: boolean;
  roundTopRight?: boolean;
  roundBottomLeft?: boolean;
  roundBottomRight?: boolean;
  nopadding?: boolean;
  disabled?: boolean;
}

export type StateProps<ST> = OwnProps & {
  mentions: Array<ST>;
}

export type DispatchProps = {
}

export type Props<ST = CustomField> = StateProps<ST> & DispatchProps;

export type State<ST = CustomField> = {
  editorState: any;
  suggestions: ST[];
};

export interface SearchChange {
  value: {};
}

export interface MentionSuggestionsProps<ST = CustomField> {
  suggestions: ST[];
  onAddMention: () => void;
  entryComponent: React.ComponentClass | React.StatelessComponent;
  onSearchChange: (value: SearchChange) => void;
  onOpen?: () => void;
}

export interface MentionPlugin {
  MentionSuggestions: React.ComponentClass<MentionSuggestionsProps> | React.StatelessComponent<MentionSuggestionsProps>;
}

export interface EditorRef {
  focus: () => void;
}