import { EditorState } from 'draft-js';
import { Map } from 'immutable';

export type DOMRectangle = ClientRect & DOMRectInit;

export interface Mention {
  has: (key: string) => boolean;
  get: (key: string) => string;
}

export interface EntryProps {
  className: string;
  onMouseDown: (event: Event) => void;
  onMouseUp: (event: Event) => void;
  onMouseEnter: (event: Event) => void;
  theme: Theme;
  mention: Mention;
  isFocused: boolean;
  searchValue: string;
  role: string;
}

// export interface ImmutableList<T> extends Array<T> {
//   toJS(): Array<T>;
// }

// export interface ImmutableMap<K, V> extends Map<K, V> {
//   map: <R = V>(fn: (item: V, key: K) => R) => ImmutableList<R>;
//   keySeq: () => K[];
//   equals: <T>(obj: T) => boolean;
// }

export interface Store<K = string, V = string> {
  getIsOpened: () => boolean;
  register: (key: string) => void;
  setIsOpened: (state: boolean) => void;
  unregister: (key: string) => void;
  updatePortalClientRect: (offset: string, fn: () => DOMRectangle) => void;
  getAllSearches: () => Map<K, V>;
  getPortalClientRect: (key: string) => DOMRectangle;
  resetEscapedSearch: () => void;
  isEscaped: (key: string) => void;
  escapeSearch: (key: string) => void;
  setEditorState: (state: EditorState) => void;
  getEditorState: () => EditorState;
}

export interface Theme {
  // CSS class for mention text
  mention?: string;
  // CSS class for suggestions component
  mentionSuggestions?: string;
  SearchEdit?: string;
  // CSS classes for an entry in the suggestions component
  mentionSuggestionsEntry?: string;
  mentionSuggestionsEntryFocused?: string;
  mentionSuggestionsEntryText?: string;
  mentionSuggestionsEntryAvatar?: string;
}

export interface Offset {
  blockKey: string;
  decoratorKey: number;
  leafKey: number;
}
