import * as React from 'react';
import * as PropTypes from 'prop-types';
import { genKey } from 'draft-js';
import { List, fromJS } from 'immutable';
import * as escapeRegExp from 'lodash.escaperegexp';
import Entry from './entry';
import addMention from '../modifiers/add_mention';
import decodeOffsetKey from '../utils/decode_offset_key';
import getSearchText from '../utils/get_search_text';
import defaultEntryComponent from './entry/default_entry_component';
import { EditorState } from 'draft-js';
import { Store, Mention, Theme, EntryProps, DOMRectangle } from '../types';
import { Map, Iterable } from 'immutable';
import { Popover, PopoverBody, Input } from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.css';

const suggestionsHoc = (Comp) => (props) => {
  if (List.isList(props.suggestions)) {
    // tslint:disable-next-line:max-line-length
    console.warn('Immutable.List for the "suggestions" prop will be deprecated in the next major version, please use an array instead'); // eslint-disable-line no-console
  }
  // if (fromJS(props.suggestions).size == 0) {
  //   return (
  //     <div>asdf</div>
  //   );
  // } else {
  //   return (
  //     < Comp
  //       {...props}
  //       suggestions={fromJS(props.suggestions)}
  //     />
  //   )
  // }
  return (
    <Comp
      {...props}
      suggestions={fromJS(props.suggestions)}
    />
  )

};

interface Callbacks {
  onClick: (event: MouseEvent) => void;
  onDownArrow: (event: KeyboardEvent) => void;
  onUpArrow: (event: KeyboardEvent) => void;
  onEscape: (event: KeyboardEvent) => void;
  handleReturn: () => void;
  onTab: (event: KeyboardEvent) => void;
  onChange: (editorState: EditorState) => void;
}

interface SuggestionProps {
  decoratorRect: DOMRectangle;
  prevProps: Props;
  prevState: State;
  props: Props;
  state: State;
  popover: HTMLElement;
}

interface Props {
  suggestions: Map<number, Mention>;
  ariaProps: {
    ariaActiveDescendantID: string;
    ariaOwneeID: string;
    ariaHasPopup: string;
    ariaExpanded: string;
  };
  onClose: () => void;
  onOpen: () => void;
  callbacks: Callbacks;
  store: Store;
  mentionTrigger: string;
  mentionPrefix: string;
  entityMutability: string;
  onAddMention: (mention: Mention) => void;
  positionSuggestions: (props: SuggestionProps) => object;
  onSearchChange: (param: { value: string }) => void;

  entryComponent: React.StatelessComponent<EntryProps> | React.ComponentClass<EntryProps>;
  popoverComponent: JSX.Element;
  theme: Theme;
}

interface State {
  isActive: boolean;
  focusedOptionIndex: number;
  isActiveSuggestioins: boolean;
  searchValue: string;
  fallbackValue: string;
  selectedMention: string;
}

export class MentionSuggestions extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      isActive: false,
      focusedOptionIndex: 0,
      isActiveSuggestioins: false,
      searchValue: '',
      fallbackValue: '',
      selectedMention: null,
    }
  }
  static propTypes = {
    entityMutability: PropTypes.oneOf([
      'SEGMENTED',
      'IMMUTABLE',
      'MUTABLE',
    ]),
    entryComponent: PropTypes.func,
    onAddMention: PropTypes.func,
    suggestions: (props, propName, componentName) => {
      if (!List.isList(props[propName])) {
        return new Error(
          `Invalid prop \`${propName}\` supplied to \`${componentName}\`. should be an instance of immutable list.`
        );
      }
      return undefined;
    },
  };
  key: string;
  popover: HTMLElement;
  activeOffsetKey: string;
  lastSelectionIsInsideWord: Iterable<string, boolean>;
  lastSearchValue: string;
  anchorElement: HTMLElement;

  componentWillMount() {
    this.key = genKey();
    this.props.callbacks.onChange = this.onEditorStateChange;
  }

  componentWillReceiveProps(nextProps: Props) {
    if (nextProps.suggestions.size === 0 && this.state.isActive) {
      this.closeDropdown();
    } else if (
      nextProps.suggestions.size > 0 && !nextProps.suggestions.equals(this.props.suggestions) &&
      !this.state.isActive
    ) {
      this.openDropdown();
    }
  }

  componentDidUpdate(prevProps: Props, prevState: State) {
    if (this.popover) {
      // In case the list shrinks there should be still an option focused.
      // Note: this might run multiple times and deduct 1 until the condition is
      // not fullfilled anymore.
      const size = this.props.suggestions.size;
      if (size > 0 && this.state.focusedOptionIndex >= size) {
        this.setState({
          focusedOptionIndex: size - 1,
        });
      }

      if (!this.props.store.getAllSearches().has(this.activeOffsetKey)) {
        return;
      }

      const decoratorRect = this.props.store.getPortalClientRect(this.activeOffsetKey);
      const newStyles = this.props.positionSuggestions({
        decoratorRect,
        prevProps,
        prevState,
        props: this.props,
        state: this.state,
        popover: this.popover,
      });
      Object.keys(newStyles).forEach((key) => {
        this.popover.style[key] = newStyles[key];
      });
    }
  }

  componentWillUnmount() {
    this.props.callbacks.onChange = undefined;
  }

  onEditorStateChange = (editorState) => {
    const searches = this.props.store.getAllSearches();

    // if no search portal is active there is no need to show the popover
    // if (searches.size === 0) {
    //   return editorState;
    // }
    const removeList = () => {
      this.props.store.resetEscapedSearch();
      this.closeDropdown();
      return editorState;
    };

    // get the current selection
    const selection = editorState.getSelection();
    const anchorKey = selection.getAnchorKey();
    const anchorOffset = selection.getAnchorOffset();

    // the list should not be visible if a range is selected or the editor has no focus
    if (!selection.isCollapsed() || !selection.getHasFocus()) {
      return removeList();
    }

    // identify the start & end positon of each search-text
    const offsetDetails = searches.map((offsetKey) => decodeOffsetKey(offsetKey));
    // a leave can be empty when it is removed due e.g. using backspace
    const leaves = offsetDetails
      .filter(({ blockKey }) => blockKey === anchorKey)
      .map(({ blockKey, decoratorKey, leafKey }) => (
        editorState
          .getBlockTree(blockKey)
          .getIn([decoratorKey, 'leaves', leafKey])
      ));

    // if all leaves are undefined the popover should be removed
    if (leaves.every((leave) => leave === undefined)) {
      return removeList();
    }

    // Checks that the cursor is after the @ character but still somewhere in
    // the word (search term). Setting it to allow the cursor to be left of
    // the @ causes troubles due selection confusion.
    const plainText = editorState.getCurrentContent().getPlainText();
    console.log("plainText:", plainText);
    const rexp = `${escapeRegExp(this.props.mentionTrigger)}`;
    console.log("rexp:", rexp);
    const rexpText = new RegExp(rexp, 'g').test(plainText);
    console.log("rexpText:", rexpText);
    const selectionIsInsideWord = leaves
      .filter((leave) => leave !== undefined)
      .map(({ start, end }) => (
        (start === 0
          && anchorOffset === this.props.mentionTrigger.length
          && plainText.charAt(anchorOffset) !== this.props.mentionTrigger
          && rexpText
          && anchorOffset <= end)
        || // @ is the first character
        (anchorOffset > start + this.props.mentionTrigger.length
          && anchorOffset <= end) // @ is in the text or at the end
      ));

    if (selectionIsInsideWord.every((isInside) => isInside === false)) {
      return removeList();
    }

    const lastActiveOffsetKey = this.activeOffsetKey;
    const filtered = selectionIsInsideWord.filter((value) => value === true);
    this.activeOffsetKey = filtered.keySeq().first();

    this.onSearchChange(editorState, selection, this.activeOffsetKey, lastActiveOffsetKey);

    // make sure the escaped search is reseted in the cursor since the user
    // already switched to another mention search
    if (!this.props.store.isEscaped(this.activeOffsetKey)) {
      this.props.store.resetEscapedSearch();
    }

    // If none of the above triggered to close the window, it's safe to assume
    // the dropdown should be open. This is useful when a user focuses on another
    // input field and then comes back: the dropdown will show again.
    const isEscaped = this.props.store.isEscaped;
    if (!this.state.isActive && !isEscaped(this.activeOffsetKey)) {
      this.openDropdown();
    }

    // makes sure the focused index is reseted every time a new selection opens
    // or the selection was moved to another mention search
    if (this.lastSelectionIsInsideWord === undefined ||
      // tslint:disable-next-line:no-string-literal
      !selectionIsInsideWord.equals(this.lastSelectionIsInsideWord)) {
      this.setState({
        focusedOptionIndex: 0,
      });
    }

    this.lastSelectionIsInsideWord = selectionIsInsideWord;

    return editorState;
  }

  onSearchChange = (editorState, selection, activeOffsetKey, lastActiveOffsetKey) => {
    const { word } = getSearchText(editorState, selection, this.props.mentionTrigger);
    const searchValue = word.substring(this.props.mentionTrigger.length, word.length);

    if (this.lastSearchValue !== searchValue || activeOffsetKey !== lastActiveOffsetKey) {
      this.lastSearchValue = searchValue;
      this.props.onSearchChange({ value: searchValue });
    }
  }
  onDownArrow = (keyboardEvent) => {
    keyboardEvent.preventDefault();
    const newIndex = this.state.focusedOptionIndex + 1;
    this.onMentionFocus(newIndex >= this.props.suggestions.size ? 0 : newIndex);
  }
  onClickInput = () => {
  }
  onClick = (mouseEvent) => {
    this.setState({
      isActiveSuggestioins: false,
      isActive: false
    });
    if (this.state.selectedMention == null) {
      return;
    }
    const newEditorState = addMention(
      this.props.store.getEditorState(),
      this.state.selectedMention,
      this.props.mentionPrefix,
      this.props.mentionTrigger,
      this.props.entityMutability,
    );
    this.props.store.setEditorState(newEditorState);
  }
  handleInput = (event) => {
    this.setState({ isActiveSuggestioins: true });
    this.setState({ searchValue: event.target.value });
    this.props.onSearchChange({ value: event.target.value });
  }
  handleFocus = () => {
    this.props.onSearchChange({ value: '' });
  }
  onTab = (keyboardEvent) => {
    keyboardEvent.preventDefault();
    this.commitSelection();
  }
  onUpArrow = (keyboardEvent) => {
    keyboardEvent.preventDefault();
    if (this.props.suggestions.size > 0) {
      const newIndex = this.state.focusedOptionIndex - 1;
      this.onMentionFocus(newIndex < 0 ? this.props.suggestions.size - 1 : newIndex);
    }
  }
  onEscape = (keyboardEvent) => {
    keyboardEvent.preventDefault();

    const activeOffsetKey = this.lastSelectionIsInsideWord
      .filter((value) => value === true)
      // tslint:disable-next-line:no-string-literal
      .keySeq()
      .first();
    this.props.store.escapeSearch(activeOffsetKey);
    this.closeDropdown();

    // to force a re-render of the outer component to change the aria props
    this.props.store.setEditorState(this.props.store.getEditorState());
  }

  onMentionSelect = (mention) => {
    if (!mention) {
      return;
    }

    // to force a re-render of the outer component to change the aria props
    this.props.store.setEditorState(this.props.store.getEditorState());
    this.setState({
      searchValue: mention.get('name'),
      fallbackValue: mention.get('capital'),
      selectedMention: mention,
      // isActiveSuggestioins: false,
    });
  }
  onMentionFocus = (index) => {
    console.log("onMentionFocus: ", index);
    const descendant = `mention-option-${this.key}-${index}`;
    this.props.ariaProps.ariaActiveDescendantID = descendant;
    this.setState({
      focusedOptionIndex: index,
    });
    // this.props.store.setEditorState(this.props.store.getEditorState());
  }

  commitSelection = () => {
    if (!this.props.store.getIsOpened()) {
      return 'not-handled';
    }

    this.onMentionSelect(this.state.selectedMention);
    return 'handled';
  }

  openDropdown = () => {
    this.props.callbacks.onClick = this.onClick;
    this.props.callbacks.onDownArrow = this.onDownArrow;
    this.props.callbacks.onUpArrow = this.onUpArrow;
    this.props.callbacks.onEscape = this.onEscape;
    this.props.callbacks.onTab = this.onTab;
    this.props.callbacks.handleReturn = this.commitSelection;
    // const descendant = `mention-option-${this.key}-${this.state.focusedOptionIndex}`;
    // this.props.ariaProps.ariaActiveDescendantID = descendant;
    // this.props.ariaProps.ariaOwneeID = `mentions-list-${this.key}`;
    // this.props.ariaProps.ariaHasPopup = 'true';
    // this.props.ariaProps.ariaExpanded = 'true';
    this.setState({
      isActive: true,
      isActiveSuggestioins: false,
      selectedMention: null,
      fallbackValue: '',
      searchValue: ''
    });

    if (this.props.onOpen) {
      this.props.onOpen();
    }
  }

  closeDropdown = () => {
    // make sure none of these callbacks are triggered
    this.props.callbacks.onClick = undefined;
    this.props.callbacks.onDownArrow = undefined;
    this.props.callbacks.onUpArrow = undefined;
    this.props.callbacks.onTab = undefined;
    this.props.callbacks.onEscape = undefined;
    this.props.callbacks.handleReturn = undefined;
    // this.props.ariaProps.ariaHasPopup = 'false';
    // this.props.ariaProps.ariaExpanded = 'false';
    // this.props.ariaProps.ariaActiveDescendantID = undefined;
    // this.props.ariaProps.ariaOwneeID = undefined;
    this.setState({
      // isActive: false,
      // isActiveSuggestioins: false,
      fallbackValue: '',
      searchValue: '',
      selectedMention: null
    });

    if (this.props.onClose) {
      this.props.onClose();
    }
  }
  render() {
    if (!this.state.isActive) {
      return null;
    }

    const {
      entryComponent,
      popoverComponent = <div />,
      theme = {},
      onClose, // eslint-disable-line no-unused-vars
      onOpen, // eslint-disable-line no-unused-vars
      onAddMention, // eslint-disable-line no-unused-vars, no-shadow
      onSearchChange, // eslint-disable-line no-unused-vars, no-shadow
      suggestions, // eslint-disable-line no-unused-vars
      ariaProps, // eslint-disable-line no-unused-vars
      callbacks, // eslint-disable-line no-unused-vars
      store, // eslint-disable-line no-unused-vars
      entityMutability, // eslint-disable-line no-unused-vars
      positionSuggestions, // eslint-disable-line no-unused-vars
      mentionTrigger, // eslint-disable-line no-unused-vars
      mentionPrefix, // eslint-disable-line no-unused-vars
      ...elementProps } = this.props;
    const entries = this.props.suggestions.map((mention: Mention, index: number) => {
      const key = mention.has('id') ? mention.get('id') : mention.get('name');
      return (
        <Entry
          key={key}
          onMentionSelect={this.onMentionSelect}
          onMentionFocus={this.onMentionFocus}
          isFocused={this.state.focusedOptionIndex === index}
          mention={mention}
          index={index}
          id={`mention-option-${this.key}-${index}`}
          theme={theme}
          searchValue={this.lastSearchValue}
          entryComponent={entryComponent || defaultEntryComponent}
        />
      );
    }).toJS();
    const dropDown =
      <div
        style={{ display: 'flex', paddingLeft: 10, paddingRight: 10 }}
      >
        <Input
          style={{ width: 150, height: 40 }}
          autoFocus
          onChange={this.handleInput}
          onFocus={this.handleFocus}
          id='VariablePopover1'
          onClick={this.onClickInput}
          value={this.state.searchValue}
        />
        <Input
          style={{ width: 150, height: 40 }}
          id='VariablePopover'
          value={this.state.fallbackValue}
          disabled
        />
        <button
          style={{ width: 100, height: 40 }}
          onClick={this.onClick}
        >Add</button>
        <Popover
          placement='bottom'
          isOpen={this.state.isActiveSuggestioins}
          target='VariablePopover'
          toggle={this.onClick}
        >
          <PopoverBody>
            {entries}
          </PopoverBody>
        </Popover>
      </div>
    return React.cloneElement(
      popoverComponent,
      {
        ...elementProps,
        className: theme.mentionSuggestions,
        role: 'listbox',
        id: `mentions-list-${this.key}`,
        ref: (element) => { this.popover = element; },
      },
      dropDown,
    );
  }
}

export default suggestionsHoc(MentionSuggestions);