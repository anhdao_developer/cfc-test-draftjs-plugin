export * from './default_entry_component';
import * as React from 'react';
import * as PropTypes from 'prop-types';
import { Mention, Theme, EntryProps } from '../../types';

interface Props {
  entryComponent: React.ComponentClass<EntryProps> | React.StatelessComponent<EntryProps>;
  index: number;
  isFocused: boolean;
  mention: Mention;
  onMentionFocus: (index: number) => void;
  onMentionSelect: (mention: Mention) => void;
  searchValue: string;
  theme: Theme;
  id: string;
}

export default class Entry extends React.Component<Props> {

  static propTypes = {
    entryComponent: PropTypes.any.isRequired,
    searchValue: PropTypes.string,
    onMentionSelect: PropTypes.func
  };

  mouseDown: boolean;

  constructor(props: Props) {
    super(props);
    this.mouseDown = false;
  }

  componentDidUpdate() {
    this.mouseDown = false;
  }

  onMouseUp = () => {
    if (this.mouseDown) {
      this.props.onMentionSelect(this.props.mention);
      this.mouseDown = false;
    }
  }

  onMouseDown = (event) => {
    // Note: important to avoid a content edit change
    event.preventDefault();

    this.mouseDown = true;
  }

  onMouseEnter = () => {
    this.props.onMentionFocus(this.props.index);
  }

  render() {
    const { theme = {}, mention, searchValue, isFocused } = this.props;
    const className = isFocused ? theme.mentionSuggestionsEntryFocused : theme.mentionSuggestionsEntry;
    const EntryComponent = this.props.entryComponent;
    return (
      <EntryComponent
        className={className}
        onMouseDown={this.onMouseDown}
        onMouseUp={this.onMouseUp}
        onMouseEnter={this.onMouseEnter}
        theme={theme}
        mention={mention}
        isFocused={isFocused}
        searchValue={searchValue}
        role="option"
      />
    );
  }
}