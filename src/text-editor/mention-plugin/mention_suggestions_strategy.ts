import findWithRegex from 'find-with-regex';
import { escapeRegExp } from 'lodash';

export default (trigger: string, regExp: string) => (
  contentBlock: Object,
  callback: Function
) => {
  // const t: ReadonlyArray<string> = `(\\s|^)${escapeRegExp(trigger)}${regExp}`;
  // const text = String.raw({
  //   raw: `(\\s|^)${escapeRegExp(trigger)}${regExp}`
  // });
  const reg = new RegExp(
    `(\\s|^)${escapeRegExp(trigger)}${regExp}`,
    'g'
  );
  findWithRegex(reg, contentBlock, callback);
};
