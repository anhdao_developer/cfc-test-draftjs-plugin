import getTypeByTrigger from './utils/get_type_by_trigger';

const findMentionEntities = trigger => (
  contentBlock,
  callback,
  contentState
) => {
  contentBlock.findEntityRanges(
    character => {
      const entityKey = character.getEntity();
      return (
        entityKey !== null &&
        contentState.getEntity(entityKey).getType() === getTypeByTrigger(trigger)
      );
    },
    callback
  );
};

export default findMentionEntities;
