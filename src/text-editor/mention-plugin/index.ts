import decorateComponentWithProps from 'decorate-component-with-props';
import { Map } from 'immutable';
import Mention from './mention';
import MentionSuggestions from './mention_suggestions'; // eslint-disable-line import/no-named-as-default
import SearchEdit from './mention_suggestions'; // eslint-disable-line import/no-named-as-default
import MentionSuggestionsPortal from './mention_suggestions_portal';
import defaultRegExp from './default_regexp';
import mentionStrategy from './mention_strategy';
import mentionSuggestionsStrategy from './mention_suggestions_strategy';
import suggestionsFilter from './utils/default_suggestions_filter';
import defaultPositionSuggestions from './utils/position_suggestions';
import { PositionProps, PositionSuggestionResults } from './utils/position_suggestions';

import { Theme, Store, DOMRectangle } from './types';

import './mention_styles.css';
import './mention_suggestions_styles.css';
import './mention_suggestions_entry_styles.css';

export { default as MentionSuggestions } from './mention_suggestions';
export { default as SearchEdit } from './SearchEdit';

class ConfigProps {
  mentionPrefix?: string;
  theme?: Theme;
  positionSuggestions?: (props: PositionProps) => PositionSuggestionResults;
  mentionComponent?: React.ComponentClass | React.StatelessComponent;
  mentionSuggestionsComponent?: React.ComponentClass | React.StatelessComponent = MentionSuggestions;
  SearchEditComponent?: React.ComponentClass | React.StatelessComponent = SearchEdit;
  entityMutability?: string = 'SEGMENTED';
  mentionTrigger?: string = '@';
  mentionRegExp?: string = defaultRegExp;
}

export default (config: ConfigProps) => {
  const defaultTheme: Theme = {
    // CSS class for mention text
    mention: 'mention',
    // CSS class for suggestions component
    mentionSuggestions: 'mentionSuggestions',
    SearchEdit: 'SearchEdit',
    // CSS classes for an entry in the suggestions component
    mentionSuggestionsEntry: 'mentionSuggestionsEntry',
    mentionSuggestionsEntryFocused: 'mentionSuggestionsEntryFocused mentionSuggestionsEntry',
    mentionSuggestionsEntryText: 'mentionSuggestionsEntryText',
    mentionSuggestionsEntryAvatar: 'mentionSuggestionsEntryAvatar',
  };

  const callbacks = {
    keyBindingFn: undefined,
    handleKeyCommand: undefined,
    onClick: undefined,
    onDownArrow: undefined,
    onUpArrow: undefined,
    onTab: undefined,
    onEscape: undefined,
    handleReturn: undefined,
    onChange: undefined,
  };

  const ariaProps = {
    ariaHasPopup: 'false',
    ariaExpanded: 'false',
    ariaOwneeID: undefined,
    ariaActiveDescendantID: undefined,
  };

  let searches = Map<string, string>();
  let escapedSearch;
  let clientRectFunctions = Map<string, () => DOMRectangle>();
  let isOpened;

  const store: Store = {
    getEditorState: undefined,
    setEditorState: undefined,
    getPortalClientRect: (offsetKey) => clientRectFunctions.get(offsetKey)(),
    getAllSearches: () => searches,
    isEscaped: (offsetKey) => {
      return escapedSearch === offsetKey;
    },
    escapeSearch: (offsetKey) => {
      escapedSearch = offsetKey;
    },

    resetEscapedSearch: () => {
      escapedSearch = undefined;
    },

    register: (offsetKey) => {
      searches = searches.set(offsetKey, offsetKey);
    },

    updatePortalClientRect: (offsetKey, func) => {
      clientRectFunctions = clientRectFunctions.set(offsetKey, func);
    },

    unregister: (offsetKey) => {
      searches = searches.delete(offsetKey);
      clientRectFunctions = clientRectFunctions.delete(offsetKey);
    },

    getIsOpened: () => isOpened,
    setIsOpened: (nextIsOpened) => { isOpened = nextIsOpened; },
  };

  // Styles are overwritten instead of merged as merging causes a lot of confusion.
  //
  // Why? Because when merging a developer needs to know all of the underlying
  // styles which needs a deep dive into the code. Merging also makes it prone to
  // errors when upgrading as basically every styling change would become a major
  // breaking change. 1px of an increased padding can break a whole layout.
  const {
    mentionPrefix = '',
    theme = defaultTheme,
    positionSuggestions = defaultPositionSuggestions,
    mentionComponent,
    mentionSuggestionsComponent = MentionSuggestions,
    SearchEditComponent = SearchEdit,
    entityMutability = 'SEGMENTED',
    mentionTrigger = '@',
    mentionRegExp = defaultRegExp,
  } = config;
  const mentionSearchProps = {
    ariaProps,
    callbacks,
    theme,
    store,
    entityMutability,
    positionSuggestions,
    mentionTrigger,
    mentionPrefix,
  };
  return {
    MentionSuggestions: decorateComponentWithProps(mentionSuggestionsComponent, mentionSearchProps),
    SearchEdit: decorateComponentWithProps(SearchEditComponent, mentionSearchProps),
    decorators: [
      {
        strategy: mentionStrategy(mentionTrigger),
        component: decorateComponentWithProps(Mention, { theme, mentionComponent }),
      },
      {
        strategy: mentionSuggestionsStrategy(mentionTrigger, mentionRegExp),
        component: decorateComponentWithProps(MentionSuggestionsPortal, { store }),
      },
    ],
    getAccessibilityProps: () => (
      {
        role: 'combobox',
        ariaAutoComplete: 'list',
        ariaHasPopup: ariaProps.ariaHasPopup,
        ariaExpanded: ariaProps.ariaExpanded,
        ariaActiveDescendantID: ariaProps.ariaActiveDescendantID,
        ariaOwneeID: ariaProps.ariaOwneeID,
      }
    ),

    initialize: ({ getEditorState, setEditorState }) => {
      store.getEditorState = getEditorState;
      store.setEditorState = setEditorState;
    },

    onClick: (mouseEvent) => callbacks.onClick && callbacks.onClick(mouseEvent),
    onDownArrow: (keyboardEvent) => callbacks.onDownArrow && callbacks.onDownArrow(keyboardEvent),
    onTab: (keyboardEvent) => callbacks.onTab && callbacks.onTab(keyboardEvent),
    onUpArrow: (keyboardEvent) => callbacks.onUpArrow && callbacks.onUpArrow(keyboardEvent),
    onEscape: (keyboardEvent) => callbacks.onEscape && callbacks.onEscape(keyboardEvent),
    handleReturn: (keyboardEvent) => callbacks.handleReturn && callbacks.handleReturn(keyboardEvent),
    onChange: (editorState) => {
      if (callbacks.onChange) {
        return callbacks.onChange(editorState);
      }
      return editorState;
    },
  };
};

export const defaultSuggestionsFilter = suggestionsFilter;