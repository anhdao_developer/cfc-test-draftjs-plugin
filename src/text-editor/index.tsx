import 'draft-js-mention-plugin/lib/plugin.css';
import * as React from 'react';
import Editor from 'draft-js-plugins-editor';
import { fromJS } from 'immutable';
import createMentionPlugin, {
  defaultSuggestionsFilter, MentionSuggestions as MentionSuggestionsComponent
} from './mention-plugin';
import { debounce, forEach } from 'lodash';
import styled from 'styled-components';
import { RichTextRaw } from './types';
import {
  Props,
  State,
  MentionPlugin,
  EditorRef,
} from './types';

import * as classnames from 'classnames';

import './styles.css';

import { EditorStateClass, EditorState, convertToRaw, convertFromRaw } from './helpers';

const EditorContainer = styled.div`
  position: relative;
  .public-DraftEditorPlaceholder-root {
    position: absolute;
    top: 1rem;
    left: 1rem;
    color: #868686;
    font-style: italic;
    cursor: text;
  }
  &.roundBottomLeft {
    .public-DraftEditor-content {
      border-bottom-left-radius: 1rem;
    }
  }
  &.roundBottomRight {
    .public-DraftEditor-content {
      border-bottom-right-radius: 1rem;
    }
  }
  &.roundTopLeft {
    .public-DraftEditor-content {
      border-top-left-radius: 1rem;
    }
  }
  &.roundTopRight {
    .public-DraftEditor-content {
      border-top-right-radius: 1rem;
    }
  }

  &.withPadding {
    .public-DraftEditor-content {
      padding: 1rem;
      border: 1px solid transparent;
      transition: border .1s ease-in;
      cursor: text !important;
      &:active, &:focus {
        border: 1px solid rgb(68, 68, 68);
      }
    }
  }
`;

const Entry = props => {
  const {
    mention,
    theme,
    searchValue, // eslint-disable-line no-unused-vars
    isFocused, // eslint-disable-line no-unused-vars
    ...parentProps
  } = props;
  return (
    <div {...parentProps} key={mention.get('id')}>
      {mention.get('name')}
    </div>
  );
};

class TextEditorComponent extends React.Component<Props, State> {
  editor: EditorRef;
  mentionPlugin: MentionPlugin = createMentionPlugin({
    entityMutability: 'IMMUTABLE',
    mentionTrigger: '{{',
    mentionSuggestionsComponent: (props) => <MentionSuggestionsComponent {...props} />
  });

  _dispatchChange = debounce(
    (editorState: EditorStateClass) => {
      const currentContent = editorState.getCurrentContent();
      const rawContent: RichTextRaw = convertToRaw(currentContent);
      this.props.onChange(rawContent);
    },
    1000
  );

  constructor(props: Props) {
    super(props);
    let editorState = EditorState.createEmpty();
    const valueInput = this.props.value || this.props.defaultValue;
    if (valueInput) {
      try {
        const rawContent = valueInput;
        forEach(rawContent.entityMap, (value, key) => {
          value.data.mention = fromJS(value.data.mention);
        });
        editorState =
          EditorState.push(
            EditorState.createEmpty(),
            convertFromRaw(rawContent),
            'update-contentState'
          );
      } catch (error) {
        console.error('TextEditor::', error);
      }
    }
    this.state = {
      editorState,
      suggestions: this.props.mentions || [],
    };
  }

  componentWillReceiveProps(nextProps: Props) {
    if (!RichTextRaw.equals(nextProps.value, this.props.value)) {
      this.setState({
        editorState: EditorState.push(
          this.state.editorState,
          convertFromRaw(nextProps.value)
        )
      });
    }
  }
  _handleChange = editorState => {
    this.setState({ editorState: editorState, });
    this._dispatchChange(editorState);
  }

  onSearchChange = ({ value }) => {
    this.setState({
      suggestions: defaultSuggestionsFilter(value, this.props.mentions || [])
    });
  }

  onAddMention = () => {
    // get the mention object selected
  }

  focus = () => {
    this.editor.focus();
  }

  handleMentionOpen = () => {
    // Do nothing?
  }

  handleEditorRef = element => {
    this.editor = element;
  }

  render() {
    const {
      roundBottomLeft,
      roundBottomRight,
      roundTopLeft,
      roundTopRight,
      nopadding,
    } = this.props;
    const { MentionSuggestions } = this.mentionPlugin;
    const plugins = [this.mentionPlugin];
    return (
      <EditorContainer
        onClick={this.focus}
        className={
          classnames({
            withPadding: !nopadding,
            roundBottomLeft,
            roundBottomRight,
            roundTopLeft,
            roundTopRight,
          })
        }
      >
        <Editor
          editorState={this.state.editorState}
          onChange={this._handleChange}
          plugins={plugins}
          ref={this.handleEditorRef}
          placeholder={this.props.placeholder || ''}
          readOnly={this.props.disabled}
        />
        <MentionSuggestions
          onSearchChange={this.onSearchChange}
          suggestions={this.state.suggestions}
          onAddMention={this.onAddMention}
          entryComponent={Entry}
          onOpen={this.handleMentionOpen}
        />
      </EditorContainer>
    );
  };
};

export const TextEditor = TextEditorComponent;