import {
  convertFromRaw as convertFromRawFn,
  convertToRaw as convertToRawFn,
  ContentState as ContentStateDRJS,
  EditorState as EditorStateDRJS,
} from 'draft-js';
import { RichTextRaw } from './types';

export class ContentStateClass extends ContentStateDRJS {
  getPlainText: () => string;
  createFromText: (text: string) => ContentStateClass;
}

export interface EditorStateClass extends EditorStateDRJS {
  createEmpty: () => EditorStateClass;
  push: (state: EditorStateClass, newContent: ContentStateClass, type?: string) => EditorStateClass;
  createWithContent: (content: ContentStateClass) => EditorStateClass;
  getCurrentContent: () => ContentStateClass;
}

export const ContentState: ContentStateClass = ContentStateDRJS;

export const EditorState: EditorStateClass = EditorStateDRJS;

export const convertFromRaw: (
  raw: RichTextRaw
) => ContentStateClass = convertFromRawFn;

export const convertToRaw: (
  input: ContentStateClass
) => RichTextRaw = convertToRawFn;
