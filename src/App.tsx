import * as React from 'react';
import './App.css';

import logo from './logo.svg';
import { TextEditor } from './text-editor';
import { RichTextRaw, ECustomFieldType } from './text-editor/types';

const mentions = [{
  id: '1',
  name: 'Australia',
  type: ECustomFieldType.Text,
  capital: 'Canberra'
}, {
  id: '2',
  name: 'Japan',
  type: ECustomFieldType.Text,
  capital: 'Tokyo'
}, {
  id: '3',
  name: 'Germany',
  type: ECustomFieldType.Text,
  capital: 'Berlin'
}, {
  id: '4',
  name: 'New Zealand',
  type: ECustomFieldType.Text,
  capital: 'Wellington'
}, {
  id: '5',
  name: 'UK',
  type: ECustomFieldType.Text,
  capital: 'London'
}];

class App extends React.Component {
  public render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <p className="App-intro">
          <TextEditor
            mentions={mentions}
            placeholder="Enter text please"
            onChange={(p) => {
            }}
            defaultValue={RichTextRaw.toRichText('')}
          />
        </p>
      </div>
    );
  }
}

export default App;
