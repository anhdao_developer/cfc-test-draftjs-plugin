import * as React from 'react';

interface Props {
    data: Array<string>;
    placeholder: string;
}

interface State {
    text: string;
    dropdownOpen: boolean;
    matched: boolean;
    searchWords: Array<string>;
}

export default class SelectInput extends React.Component<Props, State> {
    anchorElement: HTMLElement;
    constructor(props: Props) {
        super(props);
        this.state = {
            text: '',
            dropdownOpen: false,
            matched: false,
            searchWords: []
        };
        this.onClickItem = this.onClickItem.bind(this);
        this.onAddItem = this.onAddItem.bind(this);
        this.handleOnFocus = this.handleOnFocus.bind(this);
        this.handleOnBlur = this.handleOnBlur.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }
    _handleRendered = r => {
        if (!this.anchorElement) {
            this.anchorElement = r;
        }
    }
    onClickItem(index: number) {
        this.setState({
            matched: true,
            dropdownOpen: false,
            text: this.state.searchWords[index]
        });
    }
    private onAddItem = () => {
        this.setState({ dropdownOpen: false });
    }
    handleOnFocus() {
        this.setState({ dropdownOpen: true });
        let count = 0;
        let i = 0;
        let matched = false;
        for (i = 0; i < this.props.data.length; i++) {
            if (this.props.data[i] === this.state.text) {
                matched = true;
                break;
            }
        }
        const searchWords = Array<string>();
        for (i = 0; i < this.props.data.length; i++) {
            if (count === 5) {
                break;
            }
            if (this.props.data[i].includes(this.state.text)) {
                searchWords.push(this.props.data[i]);
                count = count + 1;
            }
        }
        this.setState({ searchWords });
        this.setState({ matched });
    }
    handleOnBlur(e: any) {
        if (e.relatedTarget === null || e.relatedTarget.className !== 'addSequence') {
            this.setState({ dropdownOpen: false });
        }
    }
    handleChange(event: any) {
        this.setState({ text: event.target.value });
        let count = 0;
        let i = 0;
        let matched = false;
        for (i = 0; i < this.props.data.length; i++) {
            if (this.props.data[i] === event.target.value) {
                matched = true;
                break;
            }
        }
        const searchWords = Array<string>();
        for (i = 0; i < this.props.data.length; i++) {
            if (count === 5) {
                break;
            }
            if (this.props.data[i].includes(event.target.value)) {
                searchWords.push(this.props.data[i]);
                count = count + 1;
            }
        }
        this.setState({ searchWords });
        this.setState({ matched });
    }
    public render() {
        const { text } = this.state;
        return (
            <div
                ref={this._handleRendered}
                style={{ display: 'block' }}
            >
                <input
                    type="text"
                    placeholder={this.props.placeholder}
                    style={{
                        height: 40,
                        textAlign: 'center',
                        fontSize: 16,
                        flex: 1,
                        paddingRight: '30px',
                    }}
                    value={text}
                    onChange={this.handleChange}
                    onFocus={this.handleOnFocus}
                    onBlur={this.handleOnBlur}
                />
            </div>
        );
    }
}
